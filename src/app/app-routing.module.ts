import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//page imports
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { AboutComponent } from './components/pages/about/about.component';
import { MypokemonComponent } from './components/pages/mypokemon/mypokemon.component';
import { WishlistComponent } from './components/pages/wishlist/wishlist.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';
import { PokedeetsComponent } from './components/pokedeets/pokedeets.component';

const routes: Routes = [
  { path: 'my-pokemon', component: MypokemonComponent },
  { path: 'wishlist', component: WishlistComponent },
  { path: 'about', component: AboutComponent },
  { path: '', component: PokedexComponent, pathMatch: 'full' },
  { path: ':pokeID', component: PokedeetsComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
