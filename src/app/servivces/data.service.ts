import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private http: HttpClient) {}

  // Retrieves all pokemon form the api
  getAllPokemon(url: string, limit: number = 20, offset: number = 0) {
    return this.http.get(`${url}?limit=${limit}&offset=${offset}`);
  }

  // Retrieves single pokemon details form the api
  getPokemonDetails(url: string, pokeName: string) {
    return this.http.get(`${url}/${pokeName}`);
  }
}
