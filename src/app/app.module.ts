import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { FooterComponent } from './components/footer/footer.component';
import { PokedexComponent } from './components/pokedex/pokedex.component';
import { AboutComponent } from './components/pages/about/about.component';
import { WishlistComponent } from './components/pages/wishlist/wishlist.component';
import { MypokemonComponent } from './components/pages/mypokemon/mypokemon.component';
import { PokedeetsComponent } from './components/pokedeets/pokedeets.component';
import { NotFoundComponent } from './components/pages/not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    FooterComponent,
    PokedexComponent,
    AboutComponent,
    WishlistComponent,
    MypokemonComponent,
    PokedeetsComponent,
    NotFoundComponent,
  ],
  imports: [
    NgxPaginationModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
