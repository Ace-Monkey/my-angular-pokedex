import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokedeetsComponent } from './pokedeets.component';

describe('PokedeetsComponent', () => {
  let component: PokedeetsComponent;
  let fixture: ComponentFixture<PokedeetsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokedeetsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokedeetsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
