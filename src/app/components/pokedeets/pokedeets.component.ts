import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/servivces/data.service';

@Component({
  selector: 'app-pokedeets',
  templateUrl: './pokedeets.component.html',
  styleUrls: ['./pokedeets.component.scss'],
})
export class PokedeetsComponent implements OnInit {
  pokeID: string = '';
  PokeObj: any = {};
  isWishlisted: boolean = false;
  isMyPokemon: boolean = false;
  constructor(
    private pokeDataService: DataService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getPokemonDeets();
    this.checkIfInLocalStorage();
  }

  addToMyPokemon(id: string) {
    console.log(id);
    this.appendToStorage('mypokemon', id);
  }

  addToWishlist(id: string) {
    console.log(id);
    this.appendToStorage('wishlist', id);
  }
  checkIfInLocalStorage() {
    const mypokemonLS = localStorage.getItem('mypokemon');
    const wishlistLS = localStorage.getItem('wishlist');
    if (mypokemonLS) {
      //console.log('myPM: ' + mypokemonLS.split(',').includes(this.pokeID));
      //console.log(this.pokeID);
      this.isMyPokemon = mypokemonLS.split(',').includes(this.pokeID);
    }
    if (wishlistLS) {
      //console.log('myWL: ' + wishlistLS.split(',').includes(this.pokeID));

      this.isWishlisted = wishlistLS.split(',').includes(this.pokeID);
    }
  }

  appendToStorage(name: string, data: string) {
    var old = localStorage.getItem(name);
    if (old === null) old = '';
    localStorage.setItem(name, old + data + ',');
    this.checkIfInLocalStorage();
  }

  getPokemonDeets() {
    this.route.params.subscribe((params) => {
      this.pokeID = params['pokeID'];
    });
    this.pokeDataService
      .getPokemonDetails('https://pokeapi.co/api/v2/pokemon', this.pokeID)
      .subscribe((pokeDeets) => {
        console.log(`Getting the pokedeets for id ${this.pokeID}`);
        console.dir(pokeDeets);
        this.PokeObj = pokeDeets;
      });
  }
}
