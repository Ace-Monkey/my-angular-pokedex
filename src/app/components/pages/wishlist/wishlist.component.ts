import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servivces/data.service';

@Component({
  selector: 'app-wishlist',
  templateUrl: './wishlist.component.html',
  styleUrls: ['./wishlist.component.scss'],
})
export class WishlistComponent implements OnInit {
  wishlistLS: any = [];
  constructor(private pokeDataService: DataService) {}

  ngOnInit(): void {
    this.getMyPokemon();
  }

  removeFromWishlist(id: string) {
    const wishlistLSchange = localStorage.getItem('wishlist');
    if (wishlistLSchange) {
      const myPArray = wishlistLSchange.split(',');
      const mylocalString = myPArray.filter((value) => value != id);
      const finallist = mylocalString.join(',');
      localStorage.setItem('wishlist', finallist);
      this.getMyPokemon();
    }
  }

  getMyPokemon() {
    const mypokemon = localStorage.getItem('wishlist');
    this.wishlistLS = [];
    if (mypokemon) {
      let listWithoutLast = mypokemon.split(',');
      listWithoutLast.pop();
      listWithoutLast.forEach((singleID: any) => {
        this.pokeDataService
          .getPokemonDetails('https://pokeapi.co/api/v2/pokemon', singleID)
          .subscribe((singlePokemon) => {
            this.wishlistLS.push(singlePokemon);
          });
      });
    }
  }
}
