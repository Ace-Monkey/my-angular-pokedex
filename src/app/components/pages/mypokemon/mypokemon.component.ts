import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servivces/data.service';

@Component({
  selector: 'app-mypokemon',
  templateUrl: './mypokemon.component.html',
  styleUrls: ['./mypokemon.component.scss'],
})
export class MypokemonComponent implements OnInit {
  mypokemonLS: any = [];
  constructor(private pokeDataService: DataService) {}

  ngOnInit(): void {
    this.getMyPokemon();
  }

  removeFromMyPokemon(id: string) {
    const mypokemonLSchange = localStorage.getItem('mypokemon');
    if (mypokemonLSchange) {
      const myPArray = mypokemonLSchange.split(',');
      const mylocalString = myPArray.filter((value) => value != id);
      const finallist = mylocalString.join(',');
      localStorage.setItem('mypokemon', finallist);
      this.getMyPokemon();
    }
  }

  getMyPokemon() {
    const mypokemon = localStorage.getItem('mypokemon');
    this.mypokemonLS = [];
    if (mypokemon) {
      let listWithoutLast = mypokemon.split(',');
      listWithoutLast.pop();
      listWithoutLast.forEach((singleID: any) => {
        this.pokeDataService
          .getPokemonDetails('https://pokeapi.co/api/v2/pokemon', singleID)
          .subscribe((singlePokemon) => {
            this.mypokemonLS.push(singlePokemon);
          });
      });
    }
  }
}
