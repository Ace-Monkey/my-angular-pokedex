import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/servivces/data.service';

@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex.component.html',
  styleUrls: ['./pokedex.component.scss'],
})
export class PokedexComponent implements OnInit {
  page: number = 1;
  totalPokemons: number = 0;
  pokemonList: any[] = [];
  limit: number = 20;
  errorMsg: string = '';
  constructor(private pokeDataService: DataService) {}

  ngOnInit(): void {
    this.loadPokemon();
  }

  //Grabs all pokemon to a limit
  loadPokemon() {
    this.errorMsg = '';
    this.pokemonList = [];
    console.log(`Limit: ${this.limit} + page: ${this.page}`);
    this.pokeDataService
      .getAllPokemon(
        'https://pokeapi.co/api/v2/pokemon/',
        this.limit,
        (this.page - 1) * this.limit
      )
      .subscribe((res: any) => {
        this.totalPokemons = res.count;
        res.results.forEach((result: any) => {
          this.pokeDataService
            .getPokemonDetails('https://pokeapi.co/api/v2/pokemon', result.name)
            .subscribe((singlePokemon) => {
              this.pokemonList.push(singlePokemon);
            });
        });
      });
  }

  //Search by full name functionality
  getSinglePokemon(event: any) {
    const name: string = event.target.value.toLowerCase();
    if (name === '') {
      this.loadPokemon();
      return;
    }
    this.pokeDataService
      .getPokemonDetails('https://pokeapi.co/api/v2/pokemon', name)
      .subscribe(
        (pokeDeets) => {
          this.pokemonList = [];
          this.pokemonList.push(pokeDeets);
        },
        (err: any) => {
          this.errorMsg = `No pokemon with the name '${name}' found!`;
          return;
        }
      );
  }
}
